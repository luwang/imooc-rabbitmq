package com.imooc.rabbitmq;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.imooc.rabbitmq.entity.Order;
import com.imooc.rabbitmq.producer.OrderSender;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitmqProducterApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Autowired
	private OrderSender orderSender ;
	
	@Test
	public void testSend() throws Exception {
		Order order = new Order() ;
		order.setId("201809030000000001");
		order.setName("测试订单1");
		order.setMessageId(System.currentTimeMillis() + "$" + UUID.randomUUID().toString());
		orderSender.send(order);
	}
	
}
